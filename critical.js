critical = [
  {
    "Trip": "6207311",
    "Time in Matching": "38 minutes",
    "Trip Reason": "Behavioral Health",
    "Member Name": "HERBERT LEWIS",
    "Tel.": "+12083210160",
    "Estimated Trip Mileage": "3.13 miles",
    "lng": -116.2369,
    "lat": 43.6109,
    "Link": "<a href=\"https://portal.veyo.com/trips/6207311\" target = \"_blank\" >https://portal.veyo.com/trips/6207311<\/a>"
  },
  {
    "Trip": "6229850",
    "Time in Matching": "58 minutes",
    "Trip Reason": "Behavioral Health",
    "Member Name": "FRANK CHAVEZ",
    "Tel.": "+12084654833",
    "Estimated Trip Mileage": "3.68 miles",
    "lng": -116.6225,
    "lat": 43.6187,
    "Link": "<a href=\"https://portal.veyo.com/trips/6229850\" target = \"_blank\" >https://portal.veyo.com/trips/6229850<\/a>"
  },
  {
    "Trip": "6665389",
    "Time in Matching": "22 minutes",
    "Trip Reason": "PCP",
    "Member Name": "VALERIE VARGAS",
    "Tel.": "+15622366532",
    "Estimated Trip Mileage": "20.85 miles",
    "lng": -116.627,
    "lat": 43.6095,
    "Link": "<a href=\"https://portal.veyo.com/trips/6665389\" target = \"_blank\" >https://portal.veyo.com/trips/6665389<\/a>"
  },
  {
    "Trip": "6384292",
    "Time in Matching": "57 minutes",
    "Trip Reason": "Counselor",
    "Member Name": "JOSHUA BEILHARDT",
    "Tel.": "+12085853375",
    "Estimated Trip Mileage": "10.66 miles",
    "lng": -116.6911,
    "lat": 43.6604,
    "Link": "<a href=\"https://portal.veyo.com/trips/6384292\" target = \"_blank\" >https://portal.veyo.com/trips/6384292<\/a>"
  },
  {
    "Trip": "6408436",
    "Time in Matching": "59 minutes",
    "Trip Reason": "Counselor",
    "Member Name": "LETA GARZA",
    "Tel.": "+12082502683",
    "Estimated Trip Mileage": "4.56 miles",
    "lng": -116.6941,
    "lat": 43.6025,
    "Link": "<a href=\"https://portal.veyo.com/trips/6408436\" target = \"_blank\" >https://portal.veyo.com/trips/6408436<\/a>"
  },
  {
    "Trip": "6665466",
    "Time in Matching": "34 minutes",
    "Trip Reason": "Drug Rehabilitation",
    "Member Name": "STEPHEN MILLIKIN",
    "Tel.": "+12083767083",
    "Estimated Trip Mileage": "8.39 miles",
    "lng": -116.2932,
    "lat": 43.611,
    "Link": "<a href=\"https://portal.veyo.com/trips/6665466\" target = \"_blank\" >https://portal.veyo.com/trips/6665466<\/a>"
  }
]
