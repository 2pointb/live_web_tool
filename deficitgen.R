


create_deficit_df <- function(region_id, c_val, plot = F){
  
  start <- as.POSIXlt(Sys.time(), tz = 'US/Arizona')
  startUTC <- as.POSIXlt(Sys.time(), tz = 'UTC')
  location_data <- get_live_data(start, startUTC, region_id, bbox_list)
  out <- data.frame()
  for (i in 1:length(region_id)){
    r_spikes <- create_regional_defs(start, startUTC, region_id, bbox_list, i, location_data, plot = plot)
    out <- rbind.data.frame(out, r_spikes)
  }
  
  
 
  sd <- get_SD_points()
  
  
  out <-rbind(out,sd)
  create_tally(startUTC, out)  

  l = list()
  l[['points']] = out
  l[['start_time']] = as.POSIXlt(Sys.time(), tz = 'UTC')
  l[['interval']] = 10*60
  json_format <- toJSON(l, auto_unbox = T)
  
 #write(json_format, file=paste(getwd(),"/DeficitGeneration/deficits.json", sep = ''))
  write(json_format, file = '/var/www/html/public_out/deficits.json') 
 #return(out)
}


create_tally <- function(startUTC, out){
  phx_dat <- subset(out, region == 'Phoenix')
  counts <- phx_dat$deficit
  ones <- sum(counts == 1)
  twos <- sum(counts == 2)
  threes <- sum(counts == 3)
  fours <- sum(counts >= 4)
  
  log <- read.csv('/home/sahar/DeficitGeneration/log.csv', stringsAsFactors = F)
  row <- cbind.data.frame(startUTC = as.character(startUTC), ones, twos, threes, fours)
  
  log <- rbind(log, row)
  write.csv(log, file = '/home/sahar/DeficitGeneration/log.csv', row.names = F)
  
}


# 
# create_regional_defs <- function(start, startUTC, region_id, bbox_list, reg_i, plot = plot){
#   
#   region <- region_id[[reg_i]]
#   location_data <- get_live_data(start, startUTC, region_id, bbox_list)
#   
#   supply_data <- location_data[[region]]$supply
#   demand_data <-location_data[[region]]$demand
#   
#   if(nrow(supply_data) <= 1 | nrow(demand_data) <= 1){
#     
#     blank <- data.frame(lat = numeric(),
#                       lng = numeric(),
#                       intersection = character(),
#                       deficit = numeric())
#     return(blank)
#     }
#   
#   supply_raster <- find_raster(supply_data, .015, bbox_list[[reg_i]])
#   demand_raster <- find_raster(demand_data, .015, bbox_list[[reg_i]], 1/c_val)
#   
#   delta <- find_surp_def(supply_raster, demand_raster)
#   
#   deficit_raster <- delta$deficit
#   surplus_raster <- delta$surplus
#   
#   if(plot == T){
#     map_layer <- create_map_layer(bbox_list[[reg_i]])
#     par(mar=c(4.1,4.1,5.1,2.1))
#     plot(c(xmin(map_layer), xmax(map_layer)), c(ymin(map_layer), ymax(map_layer)), type = 'n', xlab = 'Lon', ylab = 'Lat')
#     plot(map_layer, add = T)
#   }
#   
#   deficit_spikes <- plot_heatmap(deficit_raster, date, map_layer, 'Reds', region, intersections, plot = plot, neg = T)
#   
#   d_spikes <- deficit_spikes$spikes
#   
#   
#   
#   if(plot == T){
#     points(demand_data[,c('Lon', 'Lat')], col = alpha('limegreen', .6), pch = 17, cex = .9)
#     points(supply_data[,c('Lon', 'Lat')], col = alpha('blue', .6), pch = 15, cex = .9)
#     title(paste(start))
#   }
#   
#   d_spikes <- d_spikes[, c("ilat", "ilon", "inters", 'rounded')]
#   d_spikes$rounded[d_spikes$rounded == 0] <- 1
#   names(d_spikes) <-  c('lat','lng', 'intersection', 'deficit')
#   return(d_spikes)
# }



create_regional_defs <- function(start, startUTC, region_id, bbox_list, reg_i, location_data, plot = plot){
  
  region <- region_id[[reg_i]]
  #location_data <- get_live_data(start, startUTC, region_id, bbox_list)
  
  supply_data <- location_data[[region]]$supply
  demand_data <-location_data[[region]]$demand
  
  if(nrow(supply_data) <= 1 | nrow(demand_data) <= 1){
    
    blank <- data.frame(lat = numeric(),
                        lng = numeric(),
                        intersection = character(),
                        deficit = numeric(),
                        region = character())
    return(blank)
  }
  
  supply_raster <- find_raster(supply_data, .015, bbox_list[[reg_i]])
  demand_raster <- find_raster(demand_data, .015, bbox_list[[reg_i]], 1/c_val)
  
  delta <- find_surp_def(supply_raster, demand_raster)
  
  deficit_raster <- delta$deficit
  surplus_raster <- delta$surplus
  
  
  deficit_spikes <- calculate_spikes(deficit_raster, intersections, region_id, region)
  
  d_spikes <- deficit_spikes$spikes
  
  
  d_spikes <- d_spikes[, c("ilat", "ilon", "inters", 'rounded')]
  #d_spikes$rounded[d_spikes$rounded == 0] <- 1
  names(d_spikes) <-  c('lat','lng', 'intersection', 'deficit')
  d_spikes['region'] = region
  return(d_spikes)
}



calculate_spikes <- function(deficit_raster, intersections, region_id, region){
  density <- deficit_raster
  
  est.raster = raster(list(x=density$x1, y=density$x2, z=density$fhat))
  total_number <- sum(density$fhat)  
  if(is.na(total_number)){return(list(spikes = NULL))}
  
  
  spikes <- find_hot_spots(est.raster)
  tess <- find_tesselation(spikes, bbox_list[[which(region_id == region)]])
  spikes_var <- find_hot_spot_areas(tess, spikes, est.raster, bbox_list[[which(region_id == region)]])
  spikes <- assign_inters(spikes_var$spikes, intersections[[region]])
  #spikes <- spikes[order(spikes$areas, decreasing = T), ]
  spikes[['rounded']] <- ceiling(spikes$areas)
  return(list(spikes = spikes,
              w = tess))
} 

get_tucson_points <- function(start){
  #start <- as.POSIXlt(Sys.time(), tz = 'US/Arizona')
  all_pointz <- fromJSON('tucson_weekdays.js')
  hr <- hour(start)
  hr_pointz <- all_pointz[[1]]$heatpoints[[hr]]
  coords <- hr_pointz$mapPoint
  inters <- hr_pointz$intersection
  
  hr_points <- cbind(coords,inters)
  names(hr_points) <- c('lat','lng', 'intersection')
  hr_points[['deficit']] <- 1
  return(hr_points)
}





get_SD_points <- function(){
  
  
  intersection <- c('test1', 'test2', 'test3', 'test4', 'test5')
  base_lat <- 32.7157
  base_lon <- -117.1561
  lat <- rnorm(5, base_lat, .01)
  lng <- rnorm(5, base_lon, .01)
  out <- cbind.data.frame(lat,lng,intersection)
  out[['deficit']] <- ceiling(runif(5,1,8))
  out[['region']] <- 'San Diego'
  return(out)

}





def <- score_points <- function(spikes1, spikes2){
  
#   spikes1 <- demanddef_spikes$spikes
#   spikes2 <- deficit_spikes$spikes
  per_1 <- spikes1$areas/max(spikes1$areas)
  per_2 <- spikes2$areas/max(spikes2$areas)
  
  x <- sqrt(per_1) * sqrt(per_2)
  spikes1[['score']] <- x
  
  return(spikes1)
  
  
}








find_surp_def <- function(supply_raster, demand_raster){
  
  def_rast <- supply_raster
  surp_rast <- supply_raster
  delta <- supply_raster$fhat - demand_raster$fhat
  deficit <- delta
  surplus <- delta
  surplus[surplus < 0 ] <- 0 
  deficit[deficit > 0] <- 0
  deficit <- abs(deficit)
  
  
  def_rast$fhat <- deficit
  surp_rast$fhat <- surplus
  
  return(list(deficit = def_rast,
              surplus = surp_rast))
}
  
create_map_layer <- function(bbox){
  map_layer =  gmap(x = extent(bbox), lonlat = T, type = 'roadmap', style = 'saturation:-100', zoom = 10)
  return(map_layer)
}





find_raster<- function(data, b, bbox, c_val = 1){
  
  est <- bkde2D(na.omit(data[,c('Lon','Lat')]), bandwidth=b, gridsize = c(500,500), range.x = list(c(bbox[1],bbox[2]),c(bbox[3],bbox[4])))
  est$fhat <- (est$fhat/sum(est$fhat))*nrow(data)*c_val
  
  return(est)
}





find_tesselation <- function(spikes, bbox){
  z <- deldir(spikes, rw = bbox)
  w <- tile.list(z)
  return(w)
}


find_hot_spots <- function(est.raster){
  r = est.raster
  
  # x<- raster2contourPolys(r, levels = (seq(1,round(max(density$fhat)),length.out = 15)))
  
  #plot(x, add = T)
  
  
  f <- function(X) max(X, na.rm=TRUE)
  
  #   if (bandwidth[i]<70){
  #     localmax <- focal(r, w = matrix(1,3,3), fun = max, pad=FALSE, padValue=NA)
  #   }
  #   else {localmax <- focal(r, w = matrix(1,15,15), fun = max, pad=FALSE, padValue=NA)}
  # 
  localmax <- focal(r, w = matrix(1,11,11), fun = max, pad=FALSE, padValue=NA)
  ## Does each cell have the maximum value in its neighborhood?
  r2 <- r==localmax
  
  ## Get x-y coordinates of those cells that are local maxima
  maxXY <- xyFromCell(r2, Which(r2==1, cells=TRUE))
  #points(maxXY, col = 'blue', pch = 19)
  
  
  z<-extract(est.raster, maxXY)
  
  spikes <- as.data.frame(cbind(maxXY,z))
  spikes <- (spikes[order(spikes$z, decreasing = T),])
  #spikes <- (spikes[spikes$z>.01,c('x','y')])
  
  spikes <- (spikes[order(spikes$z, decreasing = T),])
  #spikes <- spikes[0:num_points,]2.1
  spikes[['a_pct']] <- spikes$z/sum(z)
  
  if(round(spikes$a_pct[1],3)==1){
    return(spikes[1,])
  }
  #spikes <- subset(spikes, cumsum(a_pct) < 0.975)
  
  out_slice_idx <- which(cumsum(spikes$a_pct) < 0.975)
  out_slice_idx <- c(out_slice_idx,(max(out_slice_idx)+1))
  #points(spikes, pch = 4, cex = 1.2, lwd = 2)
  
  spikes <- spikes[out_slice_idx,]
  
  return(spikes)
}




#demand_spikes <-  plot_heatmap(demand_raster, date, map_layer, 'Greens', region, intersections, neg = F, plot = T)


plot_heatmap <- function(raster_obj, date, map_layer, col_pal, region, intersections, neg, plot = T, def_spikes = NULL, def_poly = NULL){
  #par(oma = c(1, 0, 1, 0))
  #par(mar=c(5.1,4.1,4.1,2.1))
  density <- raster_obj
  
  est.raster = raster(list(x=density$x1, y=density$x2, z=density$fhat)) 
 
  total_number <- sum(density$fhat)  
  if(is.na(total_number)){return(list(spikes = NULL))}
  
  if(plot == T){
    
    
    #est.raster = raster(list(x=density$x1, y=density$x2, z=density$fhat)) 
    n <- 60
    colourss <- brewer.pal(9, col_pal)
    pal <- colorRampPalette(colourss)
    micolor <-  alpha(pal(n), alpha = .7)
    transp <- heat.colors(n, alpha = 0)
    micolor[1] <- transp[1]
    
    image(est.raster, col = micolor, add = T, legend = F, ext= extent(map_layer), useRaster = F)
    
  }
  
  
  if(round(total_number) >= 1 & (is.null(def_spikes) | is.null(def_poly)) | !(is.null(def_spikes) | is.null(def_poly))){
    
    if (is.null(def_spikes) | is.null(def_poly)){
      spikes <- find_hot_spots(est.raster)
      tess <- find_tesselation(spikes, bbox_list[[which(region_id == region)]])
      #inters <- find_inter_table(spikes, 10, plot_style)}
    } else {
      spikes = def_spikes
      tess = def_poly
    }
  
  spikes_var <- find_hot_spot_areas(tess, spikes, est.raster, bbox_list[[which(region_id == region)]], plot = F, plot_poly = F, neg = neg)
  spikes <- assign_inters(spikes_var$spikes, intersections[[region]])
  #spikes <- spikes[order(spikes$areas, decreasing = T), ]
  spikes[['rounded']] <- round(spikes$areas)
  return(list(spikes = spikes,
              w = tess))
  }
  else{
    #print('ok')
    #plot(1, axes = FALSE, xlab = "", ylab = "", type = "n")
    w <- NULL
    inters <- NULL
    spikes <- NULL
    return(list(spikes = spikes,
                w = w))
  }
}

# 
# 
# find_hot_spot_areas <- function(w, spikes, est.raster, bbox, plot_poly = F, plot = plot, neg = F){
#   
#   
#   areas <- c()
#   for(i in 1:length(w)){
#     poly <- SpatialPolygons(list(Polygons(list(Polygon(cbind(w[[i]]$x, w[[i]]$y))), 1)))
#     if(plot_poly == T){
#       plot(poly, add = T, lty = 3, lwd = .8)
#     }
#     
#     integ <- extract(est.raster, poly, fun = sum)
#     #print(integ)
#     
#     areas <- c(areas,integ)
#     if(integ>=1 & plot == T){
#       if(neg == T){
#         integ <- integ*-1
#       }
#       text(w[[i]]$pt[1],w[[i]]$pt[2], labels = round(integ), col = alpha('black',.7), cex = 1.5, font = 2)
#     }
#     else{
#       #points(w[[i]]$pt[1],w[[i]]$pt[2], pch = 19, cex = .3)
#     }
#   }
#   spikes[['areas']] <- areas
#   
#   return(list(spikes = spikes,
#               w = w))
# }




find_hot_spot_areas <- function(w, spikes, est.raster, bbox){
  
  
  areas <- c()
  for(i in 1:length(w)){
    poly <- SpatialPolygons(list(Polygons(list(Polygon(cbind(w[[i]]$x, w[[i]]$y))), 1)))
    
    integ <- extract(est.raster, poly, fun = sum)
    #print(integ)
    
    areas <- c(areas,integ)
    
  }
  spikes[['areas']] <- areas
  
  return(list(spikes = spikes,
              w = w))
}


find_inter_table <- function(spikes, n, plot_style){
  #plot(1, axes = FALSE, xlab = "", ylab = "", type = "n")
  col_name = paste("Forecasted Driver", plot_style)
  df2plot <- (head(spikes[,c('rounded','inters')], n))
  names(df2plot)[1] <- col_name
  names(df2plot)[2] <- 'Approximate Major Intersection'
  #addtable2plot('center', table = df2plot, cex = 1.4)
  return(df2plot)
}




create_text_string <- function(spikes, n){
  mtext(paste('Driver shortages for top', n, 'deficit regions at closest intersections:'), at = 0.65, side = 1, line = 0, cex = 1,1, outer = T)
  for(i in 1:n){
    text <- paste(spikes$inters[i],':', round(spikes$areas[i]), 'drivers ', sep = ' ')
    mtext(text, side = 1, line = (i), at = 0.5, cex = 0.7, outer = T)
  }
}

find_dist <- function(lat1, lon1, lat2, lon2){
  d <- sqrt((lat1-lat2)^2 + (lon1-lon2)^2)
  return(d)
  
}

find_inter <- function(lat, long, bank){
  d_vec<- sapply(1:nrow(bank), function(i) find_dist(lat, long, bank$lat[i],bank$lon[i]))
  inter <- as.character(bank[which.min(d_vec),'inters'])
  inter_data <- bank[which.min(d_vec),c('lat','lon','inters')]
  return(inter_data)
  
}

assign_inters <- function(spikes, bank){
  inter_data <- data.frame()
  
  for (i in 1:nrow(spikes)){
    inter_dat <- find_inter(spikes$y[i],spikes$x[i], bank)
    inter_data <- rbind(inter_data, inter_dat)
  } 
  names(inter_data) <- c('ilat','ilon','inters')
  inter_data <- cbind(spikes,inter_data)
  return(inter_data)
}



