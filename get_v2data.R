






get_matching_trips <- function(start, config){
  
  
  #' Get Trips that are in V2 matching
  #' @description Ties it all together
  #'
  
  #' @param start time stamp taken at the start of the code to keep things consistent
  #' @param config market specific variables
  #' 
  
  
  
  start_local <- with_tz(start, tzone = config$time_zone)
  start_date <- as.Date(start_local)
  
  qry1 <- "SELECT
   t.id AS TripId
    , ts.Name
    , t.RequestedPickupDate
    , ScheduledOnDemandDispatchDate
    , t.EstimatedTravelledMileage
    , t.MemberContactPhone
    , t.MemberFirstName
    , t.MemberLastName
    , pu.Coordinates.Lat AS Lat
    , pu.Coordinates.Long AS Lon
    , do.Coordinates.Lat AS DOLat
    , do.Coordinates.Long AS DOLon
    , t.DispatchStatusId
    , t.StatusId
    , trt.Name as Reason
    FROM dbo.Trip t
    
    JOIN dbo.TripReasonType trt ON trt.id=t.ReasonTypeId
    JOIN dbo.Location pu ON pu.id=t.PickupAddressId
    JOIN dbo.Location do ON do.id=t.DestinationAddressId
    JOIN dbo.TripStatus ts ON ts.id=t.StatusId
    WHERE 1=1
    and MarketId= "
  
  qry15 <- "
  and t.StatusId=2 -- unassigned
  and t.DispatchStatusId=30 -- on-demand
  and t.OnDemandStatusId=50 -- matching
  and t.IsDeleted=0
  and t.RequestedPickupDate > '"
  
  qry <- paste(qry1,config$MarketId,qry15, start_date, "'",  sep = '') 
  data <- get_from_db(qry, 'Live', db_list)
  
  data$ScheduledOnDemandDispatchDate <- as.POSIXct(data$ScheduledOnDemandDispatchDate, tz = 'UTC')
  
  poly <- get_poly(config)
  data <- filter_data_for_polygon(data, poly)
  
  data$time_in_matching <- round(difftime(data$ScheduledOnDemandDispatchDate,start, units = 'mins'))
  
  data <- subset(data, abs(time_in_matching) >= 5)
  #data <- data[,c("Lat", "Lon", "TripId")]
  return(data)
}


get_driver_data <- function(supply_data){
  
  #' get driver detail information
  #' @description given driver ids, get driver details like name, and contact information
  #'
  
  #' @param supply_data data frame with a column called id which represents a driver id
  #' 

  
  if(nrow(supply_data) == 0){return(supply_data)}
  
  ids <- supply_data$DriverId
  qry1 <- "select 
          d.id,
          u.firstname,
          u.lastname,
          u.ContactPhoneNumber
          from driver d
          inner join dbo.[User] u ON u.id=d.Id
          
          where d.drivertypeid = 1 
          and d.programstatusid in (1,2)
          --and o.name like ('Veyo Arizona')
          and d.id in ("
  
  qry <- paste(qry1, paste(ids, collapse = ', '), ')', sep = '')
  data <- get_from_db(qry, 'Live', db_list)
  data <- merge(supply_data, data, by.x = 'DriverId', by.y = 'id')
  return(data)
}


get_upcoming_trips <- function(config, start, t1, t2, t_col){
  #' Get Trips that are upcoming
  #' @description will grab a slice in time of upcoming trips that will go to the IDP pool
  #'
  
  #' @param conifg carries market related data for the query
  #' @param start initial time stamp
  #' @param t1 value in minutes as the lower bound to be added to start param
  #' @param t2 value in minutes as the upper bound to be added to start param
  #' @param t_col unnecessary variable
  #' 
  #' @examples 
  #' 
  #' get_upcoming_trips(config, start, 10, 20, t_col) 
  #' This would get you upcoming trips between 10 and 20 minutes from now (start).
  
  
  t_col <- 'ScheduledOnDemandDispatchDate'
  t1 <- start + t1*60
  t2 <- start + t2*60
  
  
  
  blah0 <- "IF OBJECT_ID('tempdb..#foo') IS NOT NULL
  BEGIN
    DROP TABLE #foo
    END
    
    ; WITH areaa AS (
    SELECT geometry::UnionAggregate(a.Polygon) AS Polygon
    FROM dbo.Area a
    WHERE 1=1
    AND a.id = "
    
    
    
    blah = "
    )
    
    SELECT 
    t.id AS TripId
    , ts.Name
    , "
    blah15 <- "
    , pu.Coordinates.Lat AS Lat
    , pu.Coordinates.Long AS Lon
    , do.Coordinates.Lat AS DOLat
    , do.Coordinates.Long AS DOLon
    , t.DispatchStatusId
    , t.EstimatedTravelledMileage
    , t.MemberContactPhone
    , t.MemberFirstName
    , t.MemberLastName
    , t.StatusId
    , isnull(trt.Name, ' ') as Reason
    , NULLIF(pu.Coordinates.Lat,0.0) AS PickupAddress_Lat
    , NULLIF(pu.Coordinates.Long,0.0) AS PickupAddress_Lon
    , pu.details as PickupAddress
    , CASE
    WHEN poly.Polygon.STIntersects(GEOMETRY::STGeomFromWKB(pu.Coordinates.STAsBinary(),4326))=1 THEN 1
    ELSE 0
    END AS PickupAddress_InPoly
    
    
    
    
    INTO #foo
    
    FROM dbo.Trip t
    Left JOIN dbo.TripReasonType trt ON trt.id=t.ReasonTypeId
    JOIN dbo.Location pu ON pu.id=t.PickupAddressId
    JOIN dbo.Location do ON do.id=t.DestinationAddressId
    JOIN dbo.TripStatus ts ON ts.id=t.StatusId
    CROSS APPLY Areaa poly
    WHERE 1=1
    and MarketId = "
  blah155 <- "
    -- and PreferredOperatorId is null
    -- and t.StatusId not in (7,24,99) 
    and t.StatusId = 2 -- unassigned
    and t.DispatchStatusId=30 -- on-demand
    and t.OnDemandStatusId in (10,15) -- que'd
    and t.IsDeleted=0
    and '"
  
  blah2 <- "' < ScheduledOnDemandDispatchDate and ScheduledOnDemandDispatchDate <='"
  
  
  
  
  blah3 <- "
  
  select * from #foo
  where PickupAddress_InPoly =1 
  "
  
  
  qry <- paste(blah0, config$AreaId, blah, t_col, blah15, config$MarketId, blah155, paste(t1), blah2, paste(t2),"'", blah3, sep = '')
  data <- get_from_db(qry, 'Live', db_list)
    
  
  #data <- data[,c("Lat", "Lon", "TripId")]
  
  data$ScheduledOnDemandDispatchDate <- as.POSIXct(data$ScheduledOnDemandDispatchDate, tz = 'UTC')
  

  data$time_until <- round(difftime(data$ScheduledOnDemandDispatchDate,start, units = 'mins'))
  
  
  
  return(data)
  
}


filter_data_for_polygon <- function(data, poly){
  
  
  #' Filter data for polygon
  #' @description given data and polygon this function returns trips whose Lat and Lon elements are contained within the polygon
  #'
  
  #' @param data data with Lat Lon elements.
  #' @param poly list of x and y coordinates comprings the polygon in question
  #' 
  
  
  idx <- point.in.polygon(data$Lon, data$Lat, poly$x, poly$y)
 
  
  poly_PU <- which(idx == 1)


  poly_data <- data[poly_PU,]
  return(poly_data)
}

get_poly <- function(config){
  
  #' Get polygon from area table
  #' @description given an AraId in the config return the corrseponding polygon as a list of x and y coordinates. 
  #' Relies on a get_poly.py function in the same working directory as specified in the config file. 
  #'
  
  #' @param config market specific variables
  #' 
  
  
  area_id <- config$AreaId
  system(paste(system('which python', intern = T), paste(config$wd, "py_poly.py",sep = ''), area_id, config$wd, sep =' '))
  poly <- read.csv(paste(config$wd, 'poly.csv', sep = ''))
  
  l = list()
  l[['x']] = poly$Longitude
  l[['y']] = poly$Latitude
  #l <- local(get(load('/home/sahar/capacity_test/fe01poly.Rdata')))  
  return(l)
}



