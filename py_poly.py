
import sys
import pandas as pd




area_id = (sys.argv)[1]
wd = (sys.argv)[2]


sys.path.append(wd)

execfile(wd + 'pyconnect.py')


area_id = str(area_id)

qry =  """DECLARE @polygon GEOMETRY

          SELECT
            @polygon = a.Polygon
          FROM dbo.Area a
          WHERE 1=1
            AND a.Id = """

qry2 = """
          DECLARE @max INT
          declare @i int = 1
          declare @points table (PointNumber int, Point GEOMETRY)

          BEGIN
            SELECT @max = max(@polygon.STNumPoints()) 
              
            while(@i<=@max)
            begin 
              insert into @points
              select @i, @polygon.STPointN(@i)
              set @i=@i+1
            END
          END

          select
            x.PointNumber AS PointOrder
            , x.Point.STY AS Latitude
            , x.Point.STX AS Longitude
          from @points x"""

master_qry = qry + area_id + qry2

poly = get_from_db(master_qry, 'Live', db_list)
poly.to_csv(wd + 'poly.csv', index = False)




