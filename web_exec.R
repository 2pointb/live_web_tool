

#

#Web script

setwd("~/")
suppressMessages(library(deldir))
suppressMessages(library(dismo))
suppressMessages(library(KernSmooth))
suppressMessages(library(raster))
suppressMessages(library(scales))
suppressMessages(library(RColorBrewer))
suppressMessages(library(jsonlite))
suppressMessages(library(lubridate))
suppressMessages(library(chron))
suppressMessages(library(mongolite))
suppressMessages(library(jsonlite))
suppressMessages(library(slackr))


slackr_setup()




source(paste(getwd(),'/live_web_tool/10mindata_web.R', sep = ''))
source(paste(getwd(),'/live_web_tool/deficitgen.R', sep = ''))
source(paste(getwd(),'/live_web_tool/connectR.R', sep = ''))
source(paste(getwd(),'/live_web_tool/web_script.R', sep = ''))
source(paste(getwd(),'/live_web_tool/get_v2data.R', sep = ''))
#load(paste(getwd(),"/live_web_tool/intersections.Rdata", sep = ''))




boise_config <- list()
boise_config[['AreaId']] <- 2043
boise_config[['MarketId']] <- 2
boise_config[['time_zone']] <- 'America/Boise'
boise_config[['name']] <- 'Boise'
boise_config[['wd']] <- paste(getwd(),"/live_web_tool/", sep ='')
boise_config[['bbox']] <- c(-116.873859, -116.044393, 43.448372, 43.800528)
boise_config[['regionid']] <- 3


tucson_config <- list()
tucson_config[['AreaId']] <- 2343
tucson_config[['MarketId']] <- 4
tucson_config[['time_zone']] <- 'US/Arizona'
tucson_config[['name']] <- 'Tucson'
tucson_config[['wd']] <- paste(getwd(),"/live_web_tool/", sep ='')
tucson_config[['regionid']] <- 1
tucson_config[['bbox']] <- c(-111.4, -110.5, 32, 32.45)


phoenix_config <- list()
phoenix_config[['AreaId']] <- 2342
phoenix_config[['MarketId']] <- 4
phoenix_config[['time_zone']] <- 'US/Arizona'
phoenix_config[['name']] <- 'Phoenix'
phoenix_config[['wd']] <- paste(getwd(),"/live_web_tool/", sep ='')
phoenix_config[['regionid']] <- 1
phoenix_config[['bbox']] <- c(-112.504423, -111.595991, 33.231453, 33.762825)


configs <- list()

configs[['Phoenix']] <- phoenix_config
configs[['Tucson']] <- tucson_config
configs[['Boise']] <- boise_config

bbox_tucson <- c(-111.4, -110.5, 32, 32.45)
bbox_phoenix <- c(-112.504423, -111.595991, 33.231453, 33.762825)
bbox_boise <- c(-116.873859, -116.044393, 43.448372, 43.800528)

bbox_list <- list()
bbox_list[['Phoenix']] <- bbox_phoenix
bbox_list[['Tucson']] <- bbox_tucson
bbox_list[['Boise']] <- bbox_boise


generate_properties(bbox_list, configs)







