offline_supply = [
  {
    "DriverId": "136399",
    "Status": "Unavailable",
    "Driver Name": "Samantha Burlingame",
    "Tel.": "+12089540961",
    "lng": -116.4274,
    "lat": 43.637,
    "Link": "<a href=\"https://portal.veyo.com/drivers/136399\" target = \"_blank\" >https://portal.veyo.com/drivers/136399<\/a>"
  },
  {
    "DriverId": "184695",
    "Status": "Unavailable",
    "Driver Name": "luan selaci",
    "Tel.": "+12083715836",
    "lng": -116.3705,
    "lat": 43.5903,
    "Link": "<a href=\"https://portal.veyo.com/drivers/184695\" target = \"_blank\" >https://portal.veyo.com/drivers/184695<\/a>"
  },
  {
    "DriverId": "198219",
    "Status": "Unavailable",
    "Driver Name": "Don Bennett",
    "Tel.": "+12084200843",
    "lng": -116.6626,
    "lat": 43.6371,
    "Link": "<a href=\"https://portal.veyo.com/drivers/198219\" target = \"_blank\" >https://portal.veyo.com/drivers/198219<\/a>"
  },
  {
    "DriverId": "198996",
    "Status": "Unavailable",
    "Driver Name": "Matthew Bosen",
    "Tel.": "+12087249697",
    "lng": -116.54,
    "lat": 43.5552,
    "Link": "<a href=\"https://portal.veyo.com/drivers/198996\" target = \"_blank\" >https://portal.veyo.com/drivers/198996<\/a>"
  },
  {
    "DriverId": "201153",
    "Status": "Unavailable",
    "Driver Name": "Kathryn Hamilton",
    "Tel.": "+12086020334",
    "lng": -116.4414,
    "lat": 43.6629,
    "Link": "<a href=\"https://portal.veyo.com/drivers/201153\" target = \"_blank\" >https://portal.veyo.com/drivers/201153<\/a>"
  },
  {
    "DriverId": "612519",
    "Status": "Unavailable",
    "Driver Name": "Truman Young",
    "Tel.": "+12085709326",
    "lng": -116.6036,
    "lat": 43.6048,
    "Link": "<a href=\"https://portal.veyo.com/drivers/612519\" target = \"_blank\" >https://portal.veyo.com/drivers/612519<\/a>"
  },
  {
    "DriverId": "905338",
    "Status": "Unavailable",
    "Driver Name": "Kathy Worwood",
    "Tel.": "+12088997207",
    "lng": -116.5675,
    "lat": 43.5412,
    "Link": "<a href=\"https://portal.veyo.com/drivers/905338\" target = \"_blank\" >https://portal.veyo.com/drivers/905338<\/a>"
  }
]
